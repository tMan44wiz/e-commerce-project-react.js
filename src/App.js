import React from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";

// Importing Material Design Bootstrap Library
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

import './App.css';

//  Importing All necessary Components
import AppHeader from "./views/header-footer/AppHeader";
import IndexPageContainer from "./views/IndexPageContainer";
import AppFooter from "./views/header-footer/AppFooter";
import CategoryPageContainer from "./views/CategoryPageContainer";
import ProductPageContainer from "./views/ProductPageContainer";
import ProductReviewContainer from "./views/ProductReviewContainer";
import CartPageContainer from "./views/CartPageContainer";

function App() {
  return (
    <Router>
        <div className="App">
            <header className="">
                        {/*<a
                  className="App-link"
                  href="https://reactjs.org"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Learn React
                </a>*/}
                <AppHeader/>
            </header>

            <section>
                <Route exact path= "/" component={ IndexPageContainer }/>
                <Route path= "/categories" component={ CategoryPageContainer } />
                <Route path= "/product" component={ ProductPageContainer } />
                <Route path= "/reviews" component={ ProductReviewContainer } />
                <Route path= "/cart" component={ CartPageContainer } />
            </section>

            <footer>
                <AppFooter/>
            </footer>
        </div>
    </Router>
  );
}

export default App;
