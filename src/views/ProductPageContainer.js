import React from 'react';


import ProductOrder from "../components/product-page/ProductOrder";
import ProductDetail from "../components/product-page/ProductDetail";
import RecentlyViewed from "../components/product-page/RecentlyViewed";

class ProductPageContainer extends React.Component {
    render() {
        return (
            <main className= "container py-5" style= {{ marginTop: "100px" }}>
                <ProductOrder />

                <br />

                <ProductDetail />

                <br />

                <RecentlyViewed />
            </main>
        );
    }
}

export default ProductPageContainer;
