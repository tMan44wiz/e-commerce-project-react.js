import React from "react";
import EmptyCart from "../components/cart-page/EmptyCart";
import NonEmptyCart from "../components/cart-page/NonEmptyCart";


const CartPageContainer = () => {

    return (
        <main className= "container py-5" style= {{ marginTop: "100px" }}>
            <EmptyCart />

            <br />

            <NonEmptyCart />
        </main>
    )
};


export default CartPageContainer;
