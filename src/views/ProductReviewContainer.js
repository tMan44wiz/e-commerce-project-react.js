import React from "react";

import ProductReviews from "../components/product-review-page/ProductReviews";


const ProductReviewContainer = () => {

    return (
        <main className= "container py-5" style= {{ marginTop: "100px" }}>
            <ProductReviews />
        </main>
    )
};

export default ProductReviewContainer;
