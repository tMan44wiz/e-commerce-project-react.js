import React from 'react';
import {MDBIcon} from "mdbreact";
import {Link} from "react-router-dom";

import './Header-Footer.css';

class AppFooter extends React.Component {
    render() {
        return (
            <footer className="app-footer">
                <div className="container">
                    <div className="row pb-4">
                        <div className="col-12 mouse bg-default">
                            <Link to= "#" className="mouse-wheel text-center"><MDBIcon icon= "arrow-up" className= "mt-3"/></Link>
                        </div>
                    </div>
                    <div className="row my-5">
                        <div className="col-md">
                            <div className="">
                                <h5 className="font-weight-bold">ShopMart</h5>
                                <MDBIcon icon= "twitter" className= "mr-2"/>
                                <MDBIcon icon= "facebook" className= "mr-2"/>
                                <MDBIcon icon= "instagram"/>
                            </div>
                        </div>
                        <div className="col-md">
                            <div className="mb-4 ml-md-5">
                                <h6 className="font-weight-bold">Menu</h6>
                                <ul className="list-unstyled">
                                    <li><Link to="#" className="py-2 d-block">Shop</Link></li>
                                    <li><Link to="#" className="py-2 d-block">About</Link></li>
                                    <li><Link to="#" className="py-2 d-block">Contact Us</Link></li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md">
                            <div className="mb-4">
                                <h6 className="font-weight-bold">Help</h6>
                                <div className="d-flex">
                                    <ul className="list-unstyled mr-l-5 pr-l-3 mr-4">
                                        <li><Link to="#" className="py-2 d-block">Shipping Information</Link></li>
                                        <li><Link to="#" className="py-2 d-block">Returns &amp; Exchange</Link></li>
                                        <li><Link to="#" className="py-2 d-block">Terms &amp; Conditions</Link></li>
                                        <li><Link to="#" className="py-2 d-block">Privacy Policy</Link></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col-md">
                            <div className="mb-4">
                                <h6 className="font-weight-bold">Have a Questions?</h6>
                                <div className="">
                                    <small className="text d-block">25A Alaba International Market, Lagos state, Nigeria</small>
                                    <span className="text d-block">+234 80 3929 2100</span>
                                    <span className="text d-block">info@yourdomain.com</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr className= "rgba-grey-strong"/>
                    <div className="row">
                        <div className="col-md-12 text-center">
                            <p>
                                Copyright &copy;
                                <script>document.write(new Date().getFullYear());</script>
                                All rights reserved <Link target="_blank" to= "#">ShopMart</Link>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default AppFooter;
