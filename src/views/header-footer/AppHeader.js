import React from 'react';
import { connect } from 'react-redux';
import {MDBBtn, MDBIcon, MDBNavbar, MDBNavbarBrand} from "mdbreact";
import {Link} from "react-router-dom";

import './Header-Footer.css';

class AppHeader extends React.Component {

    componentDidMount() {
        window.addEventListener("scroll", this.scrollHandler)
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.scrollHandler)
    }

    scrollHandler() {
        if (window.scrollY > 20) {
            //TODO If Statement.
        }
        else {
            //TODO Else Statement.
        }
    }

    render() {
        return (
            <div className= "container-fluid">
                <MDBNavbar light expand="md" fixed= "top" className= "white-text py-0 d-none d-sm-none d-md-block"
                           style={{
                               backgroundColor: "#000000"
                           }}>
                    <div className= "container">
                        <div className="col-md-3 pl-0 text-left">
                            <small className= "p-0"><MDBIcon icon= "phone" className="mr-3" />+234-80xxxxxxxx</small>
                        </div>
                        <div className="col-md-3  pl-0 text-left ">
                            <small className= "p-0"><MDBIcon icon= "envelope" className="mr-3" />info@yourdomain.com</small>
                        </div>
                        <div className="col-md-6 pl-0 text-left">
                            <small className= "p-0"><MDBIcon icon= "map-marker-alt"  className="mr-3" />25A Alaba International Market, Lagos state, Nigeria</small>
                        </div>
                    </div>
                </MDBNavbar>

                <MDBNavbar light expand="md" fixed= "top" className= "mt-sm-0 mt-md-4 pt-sm-1 pt-md-3"
                           style= {{
                               backgroundColor: this.props.isScrolled ? ("rgba(25, 25, 25, 1)") : ("#778899"),
                               //boxShadow: "none",
                           }}>
                    <div className= "container">
                        <div className="col-sm-6 col-md-3 col-lg-3 pl-0 order-md-1">
                            <Link to= "/" >
                                <MDBNavbarBrand className= "d-flex ">
                                    {/*<img src= { Logo } width= "64" alt="App Logo"/>*/}
                                    <div className= "brandLogo"/>
                                    <h4 className= "font-berkshire-swash font-weight-bold mt-1 ml-2"
                                        style= {{
                                            color: this.props.isScrolled ? ("#ffffff") : ("darkRed"),
                                        }}>
                                        ShopMart
                                    </h4>
                                </MDBNavbarBrand>
                            </Link>
                        </div>

                        <div className="col-sm-6 col-md-4 col-lg-3 m-0 order-md-3">
                            <div className="row ml-auto d-flex">
                                <Link to= "" >
                                    <div onClick= { () => {  } } className="ml-auto mr-2 "
                                         style= {{
                                             padding: "7px 5px",
                                             cursor: "pointer",
                                             color: "darkred"
                                         }}>
                                        <MDBIcon icon="user" className="mr-1"
                                                 style= {{
                                                     color: "darkred"
                                                 }}/>
                                        Login
                                    </div>
                                </Link>
                                <Link to= "/cart">
                                    <div onClick= { () => {} } className=""
                                         style= {{
                                             padding: "7px 5px",
                                             cursor: "pointer",
                                             color: "darkred",
                                         }}>
                                        <MDBIcon icon="shopping-cart" className="mr-1"
                                                 style= {{
                                                     color: "darkred"
                                                 }}/>
                                        Cart
                                        <span className= "badge red ml-2">0</span>
                                    </div>
                                </Link>
                            </div>
                        </div>

                        <div className="col-sm-12 col-md-5 col-lg-6 p-0 order-md-2">
                            <div className="md-form input-group m-0">
                                <input type="text" className="form-control"
                                       //onChange= { updateArtistQuery }
                                       //onKeyPress= { handleKeyPress }
                                       placeholder="Search for Product"
                                       aria-label="Search for Product"
                                       aria-describedby="MaterialButton-addon2"
                                />
                                <div className="input-group-append">
                                    <MDBBtn outline color=""  size="sm" type="submit" className="font-weight-bold" style= {{ padding: "10px 20px", borderRadius: 5 }}>
                                        Search
                                    </MDBBtn>
                                </div>
                            </div>
                        </div>
                    </div>
                </MDBNavbar>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isScrolled: state.isScrolled
    }
};

export default connect(mapStateToProps)(AppHeader);
