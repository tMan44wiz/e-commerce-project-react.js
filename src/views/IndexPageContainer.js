import React from "react";
import Carousel from "../components/index-page/Carousel";
import NewArrivals from "../components/index-page/NewArrivals";
import Recommended from "../components/index-page/Recommended";
import TopBrands from "../components/index-page/TopBrands";
import OurAssurance from "../components/index-page/OurAssurance";

const IndexPageContainer = () => {
    return (
        <main>
            <Carousel/>
            <NewArrivals/>
            <Recommended/>
            <TopBrands/>
            <OurAssurance/>
        </main>
    )
};

export default IndexPageContainer
