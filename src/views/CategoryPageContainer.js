import React from 'react';

import Categories from "../components/category-page/categories";
import LeftNav from "../components/category-page/left-nav";

class CategoryPageContainer extends React.Component {
    render() {
        return (
            <main className= "container py-5" style= {{ marginTop: "100px" }}>
                <div className="row">
                    <div className="col-md-7 col-lg-9 order-md-last">
                        <Categories/>
                    </div>
                    <div className="col-md-5 col-lg-3">
                        <LeftNav/>
                    </div>
                </div>
            </main>
        );
    }
}

export default CategoryPageContainer;
