import React from 'react';
import {MDBBtn} from "mdbreact";
import {Link} from "react-router-dom";

import "./ProductPage.css"

class ProductDetail extends React.Component {

    state = {
        showDescription: true,
        showReturnPolicy: false,
        showReviews: false,
    };

    showDescription = () => {
        this.setState({
            showDescription: true,
            showReturnPolicy: false,
            showReviews: false,
        })
    };

    showReturnPolicy = () => {
        this.setState({
            showDescription: false,
            showReturnPolicy: true,
            showReviews: false,
        })
    };

    showReviews = () => {
        this.setState({
            showDescription: false,
            showReturnPolicy: false,
            showReviews: true,
        })
    };


    render() {
        return (
            <main className= "container bg-white py-3">
                <div className="w-50 mx-auto">
                    <div className="row font-weight-bold text-center text-default">
                        <div className="col-sm-4">
                            <Link to= "#" onClick= { this.showDescription }>
                                <strong className="App-link pb-2 cursor">Description</strong>
                            </Link>
                        </div>
                        <div className="col-sm-4">
                            <Link to= "#" onClick= { this.showReturnPolicy }>
                                <strong className="App-link pb-2 cursor">Return policy</strong>
                            </Link>
                        </div>
                        <div className="col-sm-4">
                            <Link to= "#" onClick= { this.showReviews }>
                                <strong className="App-link pb-2 cursor">Reviews</strong>
                            </Link>
                        </div>
                    </div>
                </div>

                <hr className="w-75"/>

                <div className="row" id="description_display">
                    <div className="col-sm-12">

                        {/* PRODUCT DESCRIPTION */}
                        {(this.state.showDescription) ? (
                            <div>
                                <div>Nike Free RN 2019 iD is designed by HP <br />
                                    It's contains the following features:
                                    <ul>
                                        <li>4, 6, 8, 12 GB of RAM</li>
                                        <li>256GB, 500GB, 1TB of HDD</li>
                                        <li>15" Screen Size</li>
                                        <li>Strong Battery Life</li>
                                        <li>Fast Charging Rate</li>
                                        <li>Keyboard Light</li>
                                    </ul>

                                    The available colors are:
                                    <ul>
                                        <li>Black</li>
                                        <li>Gray/Ash</li>
                                    </ul>
                                </div>
                            </div>
                        ) : null}



                        {/* RETURN POLICY */}
                        {(this.state.showReturnPolicy) ? (
                            <div>
                                <p>Return within the space of seven days can be refund. </p>
                            </div>
                        ) : null}



                        {/* PRODUCT REVIEW */}
                        {(this.state.showReviews) ? (
                            <div>
                                <div className= "row w-75 mx-auto">
                                    <div className= "col-sm-6 border-right px-0">
                                        <strong>Average Rating</strong>
                                    </div>
                                    <div className= "col-sm-6">
                                        <strong>Yes, I have used this product before.</strong>
                                        <br />
                                        <Link to="/reviews">
                                            <MDBBtn  className= "">Write a review</MDBBtn>
                                        </Link>
                                    </div>
                                </div>

                                <br />

                                <div className="w-75 mx-auto">
                                    <div className="row">
                                        <div className="col-12">
                                            <small className="font-weight-bold">All review</small>
                                            <hr className="mt-0" />
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-sm-3">
                                            <small className="font-weight-bold d-block">Andy</small>
                                            <small className="text-black-50">Jan 12, 2020</small>
                                        </div>
                                        <div className="col-sm-9">
                                            <small className="font-weight-bold d-block">Rating Stars</small>
                                            <small>This is a nice quality polo and I love this.</small>
                                        </div>
                                    </div>
                                    <hr className="mt-0" />

                                    <div className="row">
                                        <div className="col-sm-3">
                                            <small className="font-weight-bold d-block">Richards</small>
                                            <small className="text-black-50">Jan 12, 2020</small>
                                        </div>
                                        <div className="col-sm-9">
                                            <small className="font-weight-bold d-block">Rating Stars</small>
                                            <small>This is a nice quality polo and I love the material. You can give a trial.</small>
                                        </div>
                                    </div>
                                    <hr className="mt-0" />

                                    <div className="row">
                                        <div className="col-sm-3">
                                            <small className="font-weight-bold d-block">Rose</small>
                                            <small className="text-black-50">Jan 12, 2020</small>
                                        </div>
                                        <div className="col-sm-9">
                                            <small className="font-weight-bold d-block">Rating Stars</small>
                                            <small>This is a nice quality polo and I love the material.</small>
                                        </div>
                                    </div>
                                    <hr className="mt-0" />
                                </div>
                            </div>
                        ) : null}

                    </div>
                </div>
            </main>
        );
    }
}

export default ProductDetail;
