import React from 'react';
import {MDBBtn} from "mdbreact";

import "./ProductPage.css"

import Product1 from '../../assets/img/products/product-2.png'

class ProductOrder extends React.Component {
    render() {
        return (
            <main className= "container bg-white">
                <div className="row py-3">
                    <div className="col-md-6 d-flex">
                        <img src= { Product1 } alt="" className= "img-fluid mx-auto my-auto"/>
                    </div>

                    <div className="col-md-6">
                        <h5 className= "font-weight-bold">Nike Free RN 2019 iD</h5>
                        <p>Product ID: xxxxxxxx</p>
                        <p>Brand: HP</p>

                        <hr />

                        <h4 className= "font-weight-bold">₦120.00</h4>

                        <hr />

                        <p>Colour: Black</p>

                        <p>Quantity:
                            <span className= "font-weight-bold quantity pl-2">
                                <button className= "btn-outline-grey quantityButton px-2">-</button>
                                <span className= "quantityUnit p-2">0</span>
                                <button className= "btn-outline-grey quantityButton">+</button>
                            </span>
                        </p>

                        <hr />

                        <div className="row">
                            <div className="col-lg-6">
                                <MDBBtn className= "">Add To Cart</MDBBtn>
                            </div>
                            <div className="col-lg-6 d-flex">
                                <p className= "my-auto">Save for later</p>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

export default ProductOrder;
