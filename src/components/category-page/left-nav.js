import React from 'react';
import {Link} from "react-router-dom";

import './CategoryPage.css'

class LeftNav extends React.Component {

    state = {
        showBrands: false,
        showMobile: false
    };

    showBrands = () => {
        this.setState({
            showBrands: true,
        })
    };
    showMobile = () => {
        this.setState({
            showMobile: true,
        })
    };

    render() {
        return (
            <main>
                <div className="sidebar">
                    <div className="sidebar-box-2">
                        <h2 className="heading">Categories</h2>
                        <div className="">
                            <div className="panel-heading">
                                <Link to= "#">
                                    <h5 className="panel-title" onClick= { this.showBrands }>Brands</h5>
                                </Link>
                            </div>
                            <div className="showBrands">
                                {(this.state.showBrands) ? (
                                    <ul>
                                        <li><Link to= "#">Sport</Link></li>
                                        <li><Link to= "#">Casual</Link></li>
                                        <li><Link to= "#">Running</Link></li>
                                        <li><Link to= "#">Jordan</Link></li>
                                        <li><Link to= "#">Soccer</Link></li>
                                        <li><Link to= "#">Football</Link></li>
                                        <li><Link to= "#">Lifestyle</Link></li>
                                    </ul>
                                ) : null}
                            </div>
                        </div>
                        <div className="">
                            <div className="panel-heading">
                                <Link to= "#">
                                    <h5 className="panel-title" onClick= { this.showMobile }>Mobile/Tablet</h5>
                                </Link>
                            </div>
                            <div className="showMobile">
                                {(this.state.showMobile) ? (
                                    <ul>
                                        <li><Link to= "#">Sport</Link></li>
                                        <li><Link to= "#">Casual</Link></li>
                                        <li><Link to= "#">Running</Link></li>
                                        <li><Link to= "#">Jordan</Link></li>
                                        <li><Link to= "#">Soccer</Link></li>
                                        <li><Link to= "#">Football</Link></li>
                                        <li><Link to= "#">Lifestyle</Link></li>
                                    </ul>
                                ) : null}
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

export default LeftNav;
