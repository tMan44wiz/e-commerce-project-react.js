import React from 'react';
import {MDBBtn} from "mdbreact";

import "./ProductReviews.css"

import Product1 from "../../assets/img/products/product-2.png";
import Star from "../../assets/img/icons/star.png"

class ProductReviews extends React.Component {
    render() {
        return (
            <main className= "container bg-white py-4">
                <div className="w-75 mx-auto">
                    <div className="row">
                        <div className="col-md-6 d-flex">
                            <img src= { Product1 } alt="" className= "img-fluid my-auto"/>
                        </div>
                        <div className="col-md-6">
                            <p>Nike Free RN 2019 iD</p>
                            <small className="font-weight-bold d-block">Rate it:&nbsp;&nbsp;&nbsp;&nbsp;
                                <span><img src= { Star } alt="" className= "starRating"/></span>
                            </small>
                            <small className="font-weight-bold">Review it:</small>
                            <form>
                                <div className="form-group">
                                    <small htmlFor="exampleInputEmail1">Title Of Review</small> <span
                                    className="required"> *</span>
                                    <input type="email" className="form-control" id="exampleInputEmail1"
                                           aria-describedby="emailHelp" placeholder="Title Of Review" />
                                </div>
                                <div className="form-group">
                                    <small htmlFor="reviewTextArea">Review</small> <span className="required"> *</span>
                                    <textarea className="form-control" id="reviewTextArea" placeholder="Review"
                                              rows="3" />
                                </div>
                                <MDBBtn className= "float-right">Send</MDBBtn>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

export default ProductReviews;
