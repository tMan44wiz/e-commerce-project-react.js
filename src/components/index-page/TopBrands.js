import React from 'react';

import HP_Logo from '../../assets/img/logos/hp-92.png'
import Dell_Logo from '../../assets/img/logos/dell-128.png'
import Lenovo_Logo from '../../assets/img/logos/hp-92.png'
import Samsung_Logo from '../../assets/img/logos/samsung-156.png'

class TopBrands extends React.Component {
    render() {
        return (
            <main className= "bg-light mt-5 py-5">
                <div className="container">
                    <div className="row mb-2">
                        <div className="col-md-12 text-center ftco-animate">
                            <h2 className="mb-4">Top Brands</h2>
                        </div>
                    </div>
                    <div className="row mb-5">
                        <div className="col-sm-3 d-flex">
                            <img src= { HP_Logo } alt= "HP Logo" className= "top-brands img-fluid "/>
                        </div>
                        <div className="col-sm-3 d-flex">
                            <img src= { Dell_Logo } alt= "Dell Logo" className= "top-brands img-fluid"/>
                        </div>
                        <div className="col-sm-3 d-flex">
                            <img src= { Lenovo_Logo } alt= "Lenovo Logo" className= "top-brands img-fluid"/>
                        </div>
                        <div className="col-sm-3 d-flex">
                            <img src= { Samsung_Logo } alt= "Accer Logo" className= "top-brands img-fluid"/>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

export default TopBrands;
