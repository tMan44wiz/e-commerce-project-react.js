import React from 'react';

class OurAssurance extends React.Component {
    render() {
        return (
            <main className="ftco-section ftco-no-pt ftco-no-pb">
                <div className="container">
                    <div className="row no-gutters ftco-services">
                        <div className="col-lg-4 text-center d-flex align-self-stretch ftco-animate">
                            <div className="media block-6 services p-4 py-md-5">
                                <div className="icon d-flex justify-content-center align-items-center mb-4">
                                    <span className="flaticon-bag"></span>
                                </div>
                                <div className="media-body">
                                    <h5 className="font-weight-normal">Low/Free Shipping</h5>
                                    <p>For order above ₦150000, there is a shipping discount, for orders above ₦300000,
                                        we offer a free shipping.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 text-center d-flex align-self-stretch ftco-animate">
                            <div className="media block-6 services p-4 py-md-5">
                                <div className="icon d-flex justify-content-center align-items-center mb-4">
                                    <span className="flaticon-customer-service"></span>
                                </div>
                                <div className="media-body">
                                    <h5 className="font-weight-normal">Support Customer</h5>
                                    <p>We are committed to our customers, that's why we offer a 24 hours support excluding
                                        sundays. </p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 text-center d-flex align-self-stretch ftco-animate">
                            <div className="media block-6 services p-4 py-md-5">
                                <div className="icon d-flex justify-content-center align-items-center mb-4">
                                    <span className="flaticon-payment-security"></span>
                                </div>
                                <div className="media-body">
                                    <h5 className="font-weight-normal">Secure Payments</h5>
                                    <p>No need to be worried, you shopping is guaranteed with us. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

export default OurAssurance;
