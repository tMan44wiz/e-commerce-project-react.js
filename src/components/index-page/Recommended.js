import React, {Component} from 'react';
import {Link} from "react-router-dom";

import './IndexPage.css'

import Product1 from '../../assets/img/products/product-1.png'
import Product2 from '../../assets/img/products/product-2.png'
import Product3 from '../../assets/img/products/product-3.png'
import Product4 from '../../assets/img/products/product-4.png'

class Recommended extends Component {
    render() {
        return (
            <main className= "">
                <div className="container py-5">
                    <div className="row">
                        <div className="col-md-12 text-center ftco-animate">
                            <h2 className="mb-4">Recommended for you</h2>
                        </div>
                    </div>

                    <div className="row mb-0">
                        <div className="col-sm-6 col-md-6 col-lg-3 ftco-animate mb-3">
                            <div className="card recommended_product bg-light">
                                <Link className="img-prod" to= "#">
                                    <div className="row">
                                        <div className="col-5">
                                            <img className="img-fluid ml-auto" src= { Product1 } alt="Colorlib Template"/>
                                        </div>
                                        <div className="col-7 pt-3 pl-0">
                                            <small className= "text-dark" style= {{ marginTop: "-40px" }}>Nike Free RN 2019 iD</small>
                                            <p className="price mb-0 font-weight-bold" style= {{ color: "darkred" }}><span>₦120.00</span></p>
                                            <small className="price text-dark"><span>Save ₦60.00</span></small>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        </div>
                        <div className="col-sm-6 col-md-6 col-lg-3 ftco-animate mb-3">
                            <div className="card recommended_product bg-light">
                                <Link className="img-prod" to= "#">
                                    <div className="row">
                                        <div className="col-5">
                                            <img className="img-fluid ml-auto" src= { Product2 } alt="Colorlib Template"/>
                                        </div>
                                        <div className="col-7 pt-3 pl-0">
                                            <small className= "text-dark" style= {{ marginTop: "-40px" }}>Nike Free RN 2019 iD</small>
                                            <p className="price mb-0 font-weight-bold" style= {{ color: "darkred" }}><span>₦120.00</span></p>
                                            <small className="price text-dark"><span>Save ₦60.00</span></small>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        </div>
                        <div className="col-sm-6 col-md-6 col-lg-3 ftco-animate mb-3">
                            <div className="card recommended_product bg-light">
                                <Link className="img-prod" to= "#">
                                    <div className="row">
                                        <div className="col-5">
                                            <img className="img-fluid ml-auto" src= { Product3 } alt="Colorlib Template"/>
                                        </div>
                                        <div className="col-7 pt-3 pl-0">
                                            <small className= "text-dark" style= {{ marginTop: "-40px" }}>Nike Free RN 2019 iD</small>
                                            <p className="price mb-0 font-weight-bold" style= {{ color: "darkred" }}><span>₦120.00</span></p>
                                            <small className="price text-dark"><span>Save ₦60.00</span></small>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        </div>
                        <div className="col-sm-6 col-md-6 col-lg-3 ftco-animate mb-3">
                            <div className="card recommended_product bg-light">
                                <Link className="img-prod" to= "#">
                                    <div className="row">
                                        <div className="col-5">
                                            <img className="img-fluid ml-auto" src= { Product4 } alt="Colorlib Template"/>
                                        </div>
                                        <div className="col-7 pt-3 pl-0">
                                            <small className= "text-dark" style= {{ marginTop: "-40px" }}>Nike Free RN 2019 iD</small>
                                            <p className="price mb-0 font-weight-bold" style= {{ color: "darkred" }}><span>₦120.00</span></p>
                                            <small className="price text-dark"><span>Save ₦60.00</span></small>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-lg-2">
                        <div className="col-sm-6 col-md-6 col-lg-3 ftco-animate mb-3">
                            <div className="card recommended_product bg-light">
                                <Link className="img-prod" to= "#">
                                    <div className="row">
                                        <div className="col-5">
                                            <img className="img-fluid ml-auto" src= { Product3 } alt="Colorlib Template"/>
                                        </div>
                                        <div className="col-7 pt-3 pl-0">
                                            <small className= "text-dark" style= {{ marginTop: "-40px" }}>Nike Free RN 2019 iD</small>
                                            <p className="price mb-0 font-weight-bold" style= {{ color: "darkred" }}><span>₦120.00</span></p>
                                            <small className="price text-dark"><span>Save ₦60.00</span></small>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        </div>
                        <div className="col-sm-6 col-md-6 col-lg-3 ftco-animate mb-3">
                            <div className="card recommended_product bg-light">
                                <Link className="img-prod" to= "#">
                                    <div className="row">
                                        <div className="col-5">
                                            <img className="img-fluid ml-auto" src= { Product4 } alt="Colorlib Template"/>
                                        </div>
                                        <div className="col-7 pt-3 pl-0">
                                            <small className= "text-dark" style= {{ marginTop: "-40px" }}>Nike Free RN 2019 iD</small>
                                            <p className="price mb-0 font-weight-bold" style= {{ color: "darkred" }}><span>₦120.00</span></p>
                                            <small className="price text-dark"><span>Save ₦60.00</span></small>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        </div>
                        <div className="col-sm-6 col-md-6 col-lg-3 ftco-animate mb-3">
                            <div className="card recommended_product bg-light">
                                <Link className="img-prod" to= "#">
                                    <div className="row">
                                        <div className="col-5">
                                            <img className="img-fluid ml-auto" src= { Product1 } alt="Colorlib Template"/>
                                        </div>
                                        <div className="col-7 pt-3 pl-0">
                                            <small className= "text-dark" style= {{ marginTop: "-40px" }}>Nike Free RN 2019 iD</small>
                                            <p className="price mb-0 font-weight-bold" style= {{ color: "darkred" }}><span>₦120.00</span></p>
                                            <small className="price text-dark"><span>Save ₦60.00</span></small>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        </div>
                        <div className="col-sm-6 col-md-6 col-lg-3 ftco-animate">
                            <div className="card recommended_product bg-light">
                                <Link className="img-prod" to= "#">
                                    <div className="row">
                                        <div className="col-5">
                                            <img className="img-fluid ml-auto" src= { Product2 } alt="Colorlib Template"/>
                                        </div>
                                        <div className="col-7 pt-3 pl-0">
                                            <small className= "text-dark" style= {{ marginTop: "-40px" }}>Nike Free RN 2019 iD</small>
                                            <p className="price mb-0 font-weight-bold" style= {{ color: "darkred" }}><span>₦120.00</span></p>
                                            <small className="price text-dark"><span>Save ₦60.00</span></small>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

export default Recommended;
