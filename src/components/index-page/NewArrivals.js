import React, {Component} from 'react';
import {Link} from "react-router-dom";

import './IndexPage.css'

import Product1 from '../../assets/img/products/product-1.png'
import Product2 from '../../assets/img/products/product-2.png'
import Product3 from '../../assets/img/products/product-3.png'
import Product4 from '../../assets/img/products/product-4.png'
import {MDBBtn} from "mdbreact";

class NewArrivals extends Component {
    render() {
        return (
            <main className= "container py-5">
                <div className="row justify-content-center">
                    <div className="col-md-12 text-center ftco-animate">
                        <h2 className="mb-4">New Arrival</h2>
                    </div>
                </div>

                <div className="row text-center">
                    <div className="col-sm-6 col-md-4 col-lg-3 mb-3 ftco-animate d-flex mx-auto">
                        <div className="product d-flex flex-column mx-auto">
                            <Link className="img-prod" to= "product">
                                <img className="img-fluid border" src= { Product3 } alt=""/>
                                <small className= "font-weight-bold product_name">Nike Free RN 2019 iD</small>
                                <h5 className="price text-dark font-weight-bold">₦120.00 <small className= "grey-text ml-4"><strike>₦150.00</strike></small></h5>
                                <MDBBtn outline size= "sm" className= "addToCart">Add To Cart</MDBBtn>
                            </Link>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-4 col-lg-3 mb-3 ftco-animate d-flex">
                        <div className="product d-flex flex-column mx-auto">
                            <Link className="img-prod" to= "/product">
                                <img className="img-fluid border" src= { Product2 } alt=""/>
                                <small className= "font-weight-bold product_name">Nike Free RN 2019 iD</small>
                                <h5 className="price text-dark font-weight-bold">₦120.00 <small className= "grey-text ml-4"><strike>₦150.00</strike></small></h5>
                                <MDBBtn outline size= "sm" className= "addToCart">Add To Cart</MDBBtn>
                            </Link>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-4 col-lg-3 mb-3 ftco-animate d-flex">
                        <div className="product d-flex flex-column mx-auto">
                            <Link className="img-prod" to= "product">
                                <img className="img-fluid border" src= { Product3 } alt=""/>
                                <small className= "font-weight-bold product_name">Nike Free RN 2019 iD</small>
                                <h5 className="price text-dark font-weight-bold">₦120.00 <small className= "grey-text ml-4"><strike>₦150.00</strike></small></h5>
                                <MDBBtn outline size= "sm" className= "addToCart">Add To Cart</MDBBtn>
                            </Link>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-4 col-lg-3 mb-3 ftco-animate d-flex">
                        <div className="product d-flex flex-column mx-auto">
                            <Link className="img-prod" to= "product">
                                <img className="img-fluid border" src= { Product4 } alt=""/>
                                <small className= "font-weight-bold product_name">Nike Free RN 2019 iD</small>
                                <h5 className="price text-dark font-weight-bold">₦120.00 <small className= "grey-text ml-4"><strike>₦150.00</strike></small></h5>
                                <MDBBtn outline size= "sm" className= "addToCart">Add To Cart</MDBBtn>
                            </Link>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-4 col-lg-3 mb-3 ftco-animate d-flex">
                        <div className="product d-flex flex-column mx-auto">
                            <Link className="img-prod" to= "product">
                                <img className="img-fluid border" src= { Product4 } alt=""/>
                                <small className= "font-weight-bold product_name">Nike Free RN 2019 iD</small>
                                <h5 className="price text-dark font-weight-bold">₦120.00 <small className= "grey-text ml-4"><strike>₦150.00</strike></small></h5>
                                <MDBBtn outline size= "sm" className= "addToCart">Add To Cart</MDBBtn>
                            </Link>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-4 col-lg-3 mb-3 ftco-animate d-flex">
                        <div className="product d-flex flex-column mx-auto">
                            <Link className="img-prod" to= "product">
                                <img className="img-fluid border" src= { Product3 } alt=""/>
                                <small className= "font-weight-bold product_name">Nike Free RN 2019 iD</small>
                                <h5 className="price text-dark font-weight-bold">₦120.00 <small className= "grey-text ml-4"><strike>₦150.00</strike></small></h5>
                                <MDBBtn outline size= "sm" className= "addToCart">Add To Cart</MDBBtn>
                            </Link>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-4 col-lg-3 mb-3 ftco-animate d-flex">
                        <div className="product d-flex flex-column mx-auto">
                            <Link className="img-prod" to= "product">
                                <img className="img-fluid border" src= { Product1 } alt=""/>
                                <small className= "font-weight-bold product_name">Nike Free RN 2019 iD</small>
                                <h5 className="price text-dark font-weight-bold">₦120.00 <small className= "grey-text ml-4"><strike>₦150.00</strike></small></h5>
                                <MDBBtn outline size= "sm" className= "addToCart">Add To Cart</MDBBtn>
                            </Link>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-4 col-lg-3 mb-3 ftco-animate d-flex">
                        <div className="product d-flex flex-column mx-auto">
                            <Link className="img-prod" to= "product">
                                <img className="img-fluid border" src= { Product2 } alt=""/>
                                <small className= "font-weight-bold product_name">Nike Free RN 2019 iD</small>
                                <h5 className="price text-dark font-weight-bold">₦120.00 <small className= "grey-text ml-4"><strike>₦150.00</strike></small></h5>
                                <MDBBtn outline size= "sm" className= "addToCart">Add To Cart</MDBBtn>
                            </Link>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

export default NewArrivals;
