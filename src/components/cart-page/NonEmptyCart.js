import React from 'react';

import Product1 from "../../assets/img/products/product-1.png";

class NonEmptyCart extends React.Component {
    render() {
        return (
            <main className= "bg-grey">
                <div className="row">
                    <div className="col-md-10">
                        <div className="row text-center text-light" style= {{ background: "darkRed" }}>
                            <div className="col-sm-4 text-left mt-2">
                                <h6 className= "font-weight-bold">Product Details</h6>
                            </div>
                            <div className="col-sm-2 mt-2">
                                <h6 className= "font-weight-bold">Unit Price</h6>
                            </div>
                            <div className="col-sm-3 mt-2">
                                <h6 className= "font-weight-bold">Quantity</h6>
                            </div>
                            <div className="col-sm-2 mt-2">
                                <h6 className= "font-weight-bold">Total Price</h6>
                            </div>
                            <div className="col-sm-1 mt-2">
                                <h6 className= "font-weight-bold">Delete</h6>
                            </div>
                        </div>

                        <div className="row text-center">
                            <div className="col-sm-4">
                                <div className="row py-2">
                                    <div className="col-3">
                                        <img className="img-fluid ml-auto cartProductImage" src= { Product1 } alt="Colorlib Template"/>
                                    </div>
                                    <div className="col-9 pt-1 pl-0 text-left">
                                        <p className= "text-dark my-0">Nike Free RN 2019 iD</p>
                                        <small className="">Soled by ShopMart</small>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 mt-2">₦120.00</div>
                            <div className="col-sm-3 mt-2"> - 1 +</div>
                            <div className="col-sm-2 mt-2">₦120.00</div>
                            <div className="col-sm-1 mt-2">X</div>
                        </div>

                        <hr />

                        <div className="row text-center">
                            <div className="col-sm-4">
                                <div className="row py-2">
                                    <div className="col-3">
                                        <img className="img-fluid ml-auto cartProductImage" src= { Product1 } alt="Colorlib Template"/>
                                    </div>
                                    <div className="col-9 pt-1 pl-0 text-left">
                                        <p className= "text-dark my-0">Nike Free RN 2019 iD</p>
                                        <small className="">Soled by ShopMart</small>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 mt-2">₦120.00</div>
                            <div className="col-sm-3 mt-2"> - 1 +</div>
                            <div className="col-sm-2 mt-2">₦120.00</div>
                            <div className="col-sm-1 mt-2">X</div>
                        </div>
                    </div>
                    <div className="col-md-2"></div>
                </div>
            </main>
        );
    }
}

export default NonEmptyCart;
