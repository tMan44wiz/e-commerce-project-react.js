import React from 'react';

import "./CartPage.css"

import Product1 from "../../assets/img/logos/shopping-cart-icon.png";

class EmptyCart extends React.Component {
    render() {
        return (
            <main className= "container">
                <div className="emptyCartContainer mx-auto text-center bg-white py-5">
                    <img src= { Product1 } alt="Cart Logo" className= "cartLogo mb-4"/>
                    <small className="font-weight-bold d-block">Your cart is empty.</small>
                    <small>You have not added any item to your cart.</small>
                </div>
            </main>
        );
    }
}

export default EmptyCart;
